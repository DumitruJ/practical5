library(tidyverse)
pdf("visualization.pdf")
tutor_student_correlations <- read_csv("tutor_student_correlations.csv", 
                                            col_names = FALSE)
dfTsc<-as.data.frame(t(tutor_student_correlations))
dfTsc<-dfTsc[-1,]
names(dfTsc)[1] <- "Student"
names(dfTsc)[2] <- "Tutor"
dfTsc$Student<-as.numeric(dfTsc$Student)
dfTsc$Tutor<-as.numeric(dfTsc$Tutor)
cor(dfTsc$Student,dfTsc$Tutor,use="pairwise.complete.obs")

plot(jitter(dfTsc$Tutor,1),dfTsc$Student,xlab="Tutor",ylab="Student", main="Student vs Teacher marks")
abline(lm(dfTsc$Student ~ dfTsc$Tutor))


dev.off()

